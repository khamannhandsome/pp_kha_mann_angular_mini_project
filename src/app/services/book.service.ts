import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, catchError, tap } from 'rxjs';
import { IBook } from '../ibook';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  constructor(private http: HttpClient) {}

  BASE_URL = 'api/books';

  addNewBook(books: IBook): Observable<IBook[]> {
    return this.http.post<IBook[]>(`${this.BASE_URL}`, books);
  }

  getAllBooks(): Observable<IBook[]> {
    return this.http.get<IBook[]>(this.BASE_URL).pipe(
      tap((data) => console.log('API Responses from book service:', data)),
      catchError((error) => {
        console.error('API Errord from book service:', error);
        return [];
      })
    );
  }

  deleteBookById(bookId: number): Observable<IBook> {
    return this.http.delete<IBook>(`${this.BASE_URL}/${bookId}`).pipe(
      tap((data) => console.log('API Responses:', data)),
      catchError((error) => {
        console.error('API Error:', error);
        return [];
      })
    );
  }
  getBookById(bookId: number): Observable<IBook> {
    return this.http.get<IBook>(`${this.BASE_URL}/${bookId}`);
  }

  // return this.http.put(this.productsUrl + product.id, product);
  updateBook(recievedBook: IBook, bookId: number) {
    return this.http.put(`${this.BASE_URL}/${bookId}`, recievedBook);
  }

  // for get remaining book and pass it out
  remainingBook!: IBook;
  getRemainingBook(remainingBook: IBook) {
    // this.remainingBook.next(remainingBook);
    this.remainingBook = remainingBook;
    // console.log('sfafsfa', remainingBook);
  }

  setUpdatedBook() {
    return this.remainingBook;
  }
}
