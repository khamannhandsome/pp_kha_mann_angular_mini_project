import { Injectable } from '@angular/core';
import { IBook } from '../ibook';
import {
  InMemoryBackendConfig,
  InMemoryDbService,
} from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root',
})
export class DataServiceService implements InMemoryDbService {
  constructor() {}
  createDb() {
    let books: IBook[] = [
      {
        id: 1,
        title: 'Love and redemptions',
        author: 'Kha Mann',
        category: 'Romance',
        description:
          'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet, deserunt magnam reiciendis accusamus veritatis iusto inventore repellat? Dolore maiores vitae eveniet esse, temporibus, commodi placeat ex velit exercitationem voluptatem iure officia quia. Nisi autem similique, pariatur expedita accusantium nam distinctio.',
        image: 'Group 7573-1.png',
      },

      {
        id:2,
        title: 'Love never fail',
        author: 'Sou Kai',
        category: 'Romance',
        description:
          'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet, deserunt magnam reiciendis accusamus veritatis iusto inventore repellat? Dolore maiores vitae eveniet esse, temporibus, commodi placeat ex velit exercitationem voluptatem iure officia quia. Nisi autem similique, pariatur expedita accusantium nam distinctio.',
        image: 'Group 7573-2.png',
      },
      {
        id:3,
        title: 'Ligter and Princess',
        author: 'Lorng Lorng',
        category: 'Romance',
        description:
          'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet, deserunt magnam reiciendis accusamus veritatis iusto inventore repellat? Dolore maiores vitae eveniet esse, temporibus, commodi placeat ex velit exercitationem voluptatem iure officia quia. Nisi autem similique, pariatur expedita accusantium nam distinctio.',
        image: 'Group 7573-3.png',
      },
      {
        id: 4,
        title: 'Love is better than immotality',
        author: 'Chou Chen',
        category: 'Romance',
        description:
          'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet, deserunt magnam reiciendis accusamus veritatis iusto inventore repellat? Dolore maiores vitae eveniet esse, temporibus, commodi placeat ex velit exercitationem voluptatem iure officia quia. Nisi autem similique, pariatur expedita accusantium nam distinctio.',
        image: 'Group 7573-4.png',
      },
      {
        id: 5,
        title: 'You are my breath',
        author: 'Chen Xue Xee',
        category: 'Romance',
        description:
          'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet, deserunt magnam reiciendis accusamus veritatis iusto inventore repellat? Dolore maiores vitae eveniet esse, temporibus, commodi placeat ex velit exercitationem voluptatem iure officia quia. Nisi autem similique, pariatur expedita accusantium nam distinctio.',
        image: 'Group 7573.png',
      },
      {
        id: 6,
        title: 'Hometown Cha Cha Cha',
        author: 'Kha Mann',
        category: 'Romance',
        description:
          'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet, deserunt magnam reiciendis accusamus veritatis iusto inventore repellat? Dolore maiores vitae eveniet esse, temporibus, commodi placeat ex velit exercitationem voluptatem iure officia quia. Nisi autem similique, pariatur expedita accusantium nam distinctio.',
        image: 'Group 7575.png',
      },
    ];
    return { books };
  }
}
