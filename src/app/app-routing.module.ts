import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './components/homepage/homepage.component';
import { BookComponent } from './components/book/book.component';
import { LoginComponent } from './components/login/login.component';
import { BookpageComponent } from './components/bookpage/bookpage.component';
import { AddbookComponent } from './components/addbook/addbook.component';
import { authGuard } from './auth.guard';
import { CategoryComponent } from './category/category.component';

const routes: Routes = [
  
  
  {
    path:'', component: HomepageComponent
  },

  {
    path:'homepage', component: HomepageComponent
  },
 
  {
    path:'book', component:BookComponent,
    children: [{path: 'category',component: CategoryComponent}]

    ,canActivate: [authGuard]

  },
  {
    path:'book/:bookId', component:BookpageComponent
    ,canActivate: [authGuard]

  },

  {
    path:'login', component:LoginComponent, 
    //  canActivate:[authGuard]

  },
  // {
  //   path:'bookpage', component:BookpageComponent
  // },
  {
    path:'addbook', component:AddbookComponent
    ,canActivate: [authGuard]

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
