export interface IBook {
    id:number;
    title: string;
    author: string;
    category: string;
    description: string;
    image: string;
}
