import {
  Component,
  OnInit,
  ViewChild,
  DoCheck,
  OnChanges,
  SimpleChanges,
  AfterViewInit,
  AfterViewChecked,
} from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IBook } from 'src/app/ibook';
import { BookService } from 'src/app/services/book.service';
import { PopupModalComponent } from '../popup-modal/popup-modal.component';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css'],
})
export class HomepageComponent implements OnInit {
  constructor(private router: Router, private bookService: BookService) {
    this.checkToken();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.getAllBooks();
  }

  ngOnInit() {
    this.getAllBooks();
  }

  isLogin: boolean = false;
  checkToken() {
    let lol = localStorage.getItem('token');
    console.log('token', lol);
    if (lol) {
      this.isLogin = true;
    } else {
      this.isLogin = false;
    }
  }

  books: IBook[] = [];

  homeClick() {
    this.router.navigate(['homepage']);
  }
  BookClick() {
    this.router.navigate(['book']);
  }
  LogoutClick() {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
    console.log(localStorage.getItem('token'));
  }

  getAllBooks() {
    return this.bookService.getAllBooks().subscribe((books) => {
      console.log(books);

      this.books = books;
    });
  }

  deleteBookClick(bookId: number, index: number) {
    return this.bookService.deleteBookById(bookId).subscribe((books) => {
      this.books.splice(index, 1);
      console.log(books, bookId);
      console.log('lol', this.books);
    });
  }

  getBookById(bookId: number) {
    return this.bookService.getBookById(bookId).subscribe((books) => {
      this.router.navigate(['book', bookId]);
    });
  }

  showModal: boolean = false;

  receivedData!: boolean;

  receiveData(data: IBook) {
    console.log('received data ', data);

    this.showModal = false;

    return this.bookService.updateBook(data, data.id).subscribe((data) => {
      console.log(data);
      this.getAllBooks();
    });
  }

  recievedBook!: IBook;

  openModal(bookId: number) {
    this.getAllBooks();
    console.log('lollllllllllllllllllll', this.getAllBooks);
    this.showModal = true;

    // this.bookService.updateBook(this.recievedBook,bookId).subscribe(data =>{
    //   this.getAllBooks();
    // })
    //   console.log('dsdsadasasdasdasdadaddasdasdasd',this.recievedBook)
  }

  sendingRemainingBook(remainBook: IBook) {
    this.showModal = true;

    this.bookService.getRemainingBook(remainBook);
  }

  ngDoCheck(): void {
    // this.getAllBooks();
  }
  recieveUpdatedBook(e: IBook) {
    this.recievedBook = e;
    console.log('recievedBook', e);
  }

  // updateBook(bookId:number){

  //   return this.bookService.updateBook(this.recievedBook,bookId).subscribe(data =>{
  //     this.getAllBooks();

  //   })
  // }
}
