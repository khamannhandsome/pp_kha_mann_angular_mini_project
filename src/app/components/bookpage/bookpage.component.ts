import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IBook } from 'src/app/ibook';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-bookpage',
  templateUrl: './bookpage.component.html',
  styleUrls: ['./bookpage.component.css'],
})
export class BookpageComponent implements OnInit {
  constructor(
    private router: Router,
    private bookService: BookService,
    private activatedRoute: ActivatedRoute
  ) {}

  idFromRoute!: any | undefined;
  // book!: IBook 

  ngOnInit(): void {
    this.getBooks();
  
  }

  backToHome() {
    this.router.navigate(['homepage']);
  }


  displayBook!: IBook;

  getBooks() {
   let id = +this.activatedRoute.snapshot.paramMap.get('bookId')!;

    this.bookService.getAllBooks().subscribe((res) => {
      res.map((book) =>{
        if(book.id === id){
          this.displayBook=book
        }
      })
    });
  }

  // testClick() {
  //   console.log(this.book);
  // }

  getBookByIdFromRoute() {
    // this.idFromRoute = +this.activatedRoute.snapshot.paramMap.get('id')!;
    // this.bookService.getAllBooks().subscribe((books) => (books = books));

  
    // for(let book of this.books ){
    //   console.log('books',this.books)
    //   console.log('fsfafsafa',book)
    //     if(this.idFromRoute=== book.id){
    //       this.displayBook=book;
    //       console.log('fsafasf',this.displayBook)
    //     }
    //   // console.log('first')
    // }
  }
}
