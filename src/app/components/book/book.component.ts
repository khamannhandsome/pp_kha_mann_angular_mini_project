import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { IBook } from 'src/app/ibook';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css'],
})
export class BookComponent implements OnInit {
  constructor(private router: Router, private bookService: BookService) {}


  showChildRoute = false;

  ngOnInit(): void {
    this.getAllBooks();

      // Subscribe to the route events to determine when to show the router-outlet
      this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          // Check if the current route has children (e.g., "/book/child-route")
          this.showChildRoute = this.router.url.includes('/book/');
        }
      });
  }

  books: IBook[] = [];

  homeClick() {
    this.router.navigate(['homepage']);
  }
  BookClick() {
    this.router.navigate(['book']);
  }
  LoginClick() {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }
  addBookClick() {
    this.router.navigate(['addbook']);
  }

  getAllBooks() {
    this.bookService.getAllBooks().subscribe((books) => {
      this.books = books;
    });
  }

  deleteBookClick(bookId: number, index: number) {
    return this.bookService.deleteBookById(bookId).subscribe((books) => {
      this.books.splice(index, 1);
      console.log(books, bookId);
      console.log('lol', this.books);
    });
  }

  getBookById(bookId: number) {
    return this.bookService.getBookById(bookId).subscribe((books) => {
      this.router.navigate(['book', bookId]);
    });
  }

  showModal: boolean = false;
  sendingRemainingBook(remainBook: IBook) {
    this.showModal = true;

    this.bookService.getRemainingBook(remainBook);
  }

  receiveData(data: IBook) {
    console.log('received data ', data);

    this.showModal = false;

    return this.bookService.updateBook(data, data.id).subscribe((data) => {
      console.log(data);
      this.getAllBooks();
    });
  }
}
