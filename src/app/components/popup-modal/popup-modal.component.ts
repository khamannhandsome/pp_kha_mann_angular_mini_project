import { Component, EventEmitter, Input, Output ,OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IBook } from 'src/app/ibook';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-popup-modal',
  templateUrl: './popup-modal.component.html',
  styleUrls: ['./popup-modal.component.css']
})
export class PopupModalComponent implements OnInit {
constructor(private formBuilder: FormBuilder, private bookService: BookService){}
  showModal: boolean = false;

  openModal() {
    this.showModal = true;
    console.log('whyyyyy')
  }

  closeModal() {
    this.showModal = false;
  }

  ngOnInit(): void {
      this.bookService.setUpdatedBook();
    this.initForm();

    // this.getAllBooks()
    // console.log(this.articleForm);
  }
  // books: IBook[] = [];
  bookForm!: FormGroup;
  imageUrl: string= '../../../assets/book1.jpg';

  private initForm() {
    let gss = this.bookService.setUpdatedBook();
    // console.log(gss)
    this.bookForm = this.formBuilder.group({
      id: [gss.id, Validators.required],
      title: [gss.title, Validators.required],
      author: [gss.author, Validators.required],
      category: ['', Validators.required],
      description: [gss.description, Validators.required],
      image:[gss.image,Validators.required]
    });
  }
  setUpdatedBook(){

    let gss : IBook ; 
    gss = this.bookService.setUpdatedBook();

    console.log(this.bookService.setUpdatedBook())

    this.bookForm.setValue({
      id: gss.id,
      title: gss.title,
      author: gss.author,
      category:gss.category,
      description: gss.description,
      image:gss.image

    });
  }
    // for closing the modal
  @Output() dataEmitter = new EventEmitter();

  sendDataToParent() {
 
    const dataToSend = true;
    this.dataEmitter.emit(dataToSend);
    console.log('niceeeeeeeeeeeeeee')

    
  }
updatedBook!: IBook | undefined; 


  @Output() updatedBookEmitter = new EventEmitter<any>();

  getAllBooks(){
    this.bookService.getAllBooks().subscribe(books => {
      console.log('whyyyyyyyyyyyyyy',books)
    })
  }
 
    updateBook(){
      this.updatedBook= this.bookForm.value;

      // console.log('this.bookForm.value',this.bookForm.value)
      // this.updatedBookEmitter.emit(this.updatedBook);
      //   console.log('asdsadsada',this.updatedBook)
      let gss : IBook ; 
      gss = this.bookService.setUpdatedBook();
      console.log('haha',this.bookForm.value);
      const dataToSend = true;
      this.dataEmitter.emit(this.bookForm.value);


      console.log('sfafniaiovsiovioio ',gss.id)
      console.log('ddd: ',this.bookForm.value)
     
        return this.bookService.updateBook(this.bookForm.value,gss.id).subscribe(data =>{
        // console.log('this.getAllBooks();',this.getAllBooks())
        // this.getAllBooks();
console.log('data',data)
         })
         }        


    

      
  }
