import { Component,OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private router : Router){}
  formGroup!: FormGroup;


  ngOnInit(): void {
    this.formGroup = new FormGroup({

        email: new FormControl(null, [Validators.required, Validators.pattern('.*_.*@gmail\.com')]),
        password: new FormControl(null, [Validators.required , Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$")]),
    
    }) }


  onLogin() {
    if (this.formGroup.valid) {
      localStorage.setItem('token', Math.random().toString());
      this.router.navigate(['homepage']);
    }else{
      // alert('fafafa')
        console.log("Please enter the valid password!");
    }
   
  }

  showPassword: boolean = false;

  togglePasswordVisibility() {
    this.showPassword = !this.showPassword;
  }



}
