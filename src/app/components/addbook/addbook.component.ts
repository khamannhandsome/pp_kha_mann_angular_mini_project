import { Component, OnInit ,OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IBook } from 'src/app/ibook';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent {

  constructor( private router: Router , private bookService: BookService,private formBuilder: FormBuilder){}

  ngOnInit(): void {
    this.initForm();
    this.getAllBooks()
    // console.log(this.articleForm);
  }
  books: IBook[] = [];
  bookForm!: FormGroup;
  imageUrl: string= '../../../assets/image 19.png';

  private initForm() {
    this.bookForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      category: ['', Validators.required],
      description: ['', Validators.required],
      image:[this.imageUrl,Validators.required]
    });
  }


  backToHome(){
    this.router.navigate(['homepage'])
    console.log('gaagag')

  }

getAllBooks(){
  this.bookService.getAllBooks().subscribe((books)=>{
    console.log("sadadadasd",books)
    this.books = books
  })
}

addNewBook(){
  let bookObject : IBook =this.bookForm.value;
  this.bookService.addNewBook(bookObject).subscribe(book=>{
    console.log('books',book)
    this.getAllBooks();

    this.router.navigate(['book'])
  })
}




}
