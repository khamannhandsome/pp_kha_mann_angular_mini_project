import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { BookComponent } from './components/book/book.component';
import { LoginComponent } from './components/login/login.component';
import { BookpageComponent } from './components/bookpage/bookpage.component';
import { AddbookComponent } from './components/addbook/addbook.component';
import {  HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DataServiceService } from './services/data-service.service';
import { ReactiveFormsModule } from '@angular/forms';
import { PopupModalComponent } from './components/popup-modal/popup-modal.component';
import { CategoryComponent } from './category/category.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    BookComponent,
    LoginComponent,
    BookpageComponent,
    AddbookComponent,
    PopupModalComponent,
    CategoryComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(DataServiceService),
    ReactiveFormsModule
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
